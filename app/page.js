"use client";

import { Button } from "@/components/ui/button";
import { Dialog, DialogTrigger, DialogContent } from "@/components/ui/dialog";

export default function Home() {
  return (
    <div className="h-screen flex flex-col">
      <div className="header h-14 border-b border-border/40"></div>
      <div className="flex-1">
        <div className="container">
          <div className="py-4">
            <Dialog>
              <DialogTrigger asChild>
                <Button size="sm">Open Dialog</Button>
              </DialogTrigger>
              <DialogContent onOpenAutoFocus={(e) => e.preventDefault()}>
                This is a Dialog
              </DialogContent>
            </Dialog>
          </div>
        </div>
      </div>
    </div>
  );
}
